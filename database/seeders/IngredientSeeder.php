<?php

namespace Database\Seeders;

use App\Models\Ingredient;
use Illuminate\Database\Seeder;

class IngredientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $ingredients = [
            ['name' => 'beef', 'initial_stock' => '20000', 'current_stock' => '20000'],
            ['name' => 'cheese', 'initial_stock' => '5000', 'current_stock' => '5000'],
            ['name' => 'onion', 'initial_stock' => '1000', 'current_stock' => '1000'],
        ];

        collect($ingredients)->each(
            fn($ingredient) => Ingredient::firstOrCreate(['name' => $ingredient['name']], $ingredient)
        );
    }
}
