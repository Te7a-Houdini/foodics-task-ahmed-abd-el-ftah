<?php

namespace Database\Seeders;

use App\Models\Ingredient;
use App\Models\Product;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        if (!Product::count()) {
            DB::transaction(function () {
                $product = Product::create(['name' => 'burger']);

                collect([
                    ['name' => 'beef', 'amount' => 150],
                    ['name' => 'cheese', 'amount' => 30],
                    ['name' => 'onion', 'amount' => 20],
                ])->each(function ($ingredient) use ($product) {
                    $product->ingredients()
                        ->syncWithoutDetaching(
                            [
                                Ingredient::where('name', $ingredient['name'])->first()->id => ['amount' => $ingredient['amount']]
                            ]
                        );
                });
            });
        }
    }
}
