## Foodics Task (Ahmed Abd El Ftah)

## Installation
- git clone git@gitlab.com:Te7a-Houdini/foodics-task-ahmed-abd-el-ftah.git
- cp .env.example .env
- configure .env file and add DB credentials
- composer install
- npm install
- php artisan migrate
- php artisan key:generate
- php artisan serve

## To Run Tests

- php artisan test

## Decisions

- i have followed laravel conventions in naming (controllers, models, tables , etc)
- for DB structure i have used **initial_stock** & **current_stock** inside **ingredients table** so that i can track when a stock level reaches below 50% . and i didn't want to over-engineer the task but if we wanted to track when a merchant had restock at a certain time and show a chart with times of when stock was low or high that approach in db structure won't work. but i did it her cause it suits the requirements and scope of the task .
- warning mail is **queued** by default so that we don't halt the system and make the current request when ordering slow . so queues made a perfect sense for this use case.
- for the readability and debugging of code i have added multiple methods to **Product model** & **Ingredient model** so that the controller be as thin as possible and doesn't contain much logic.
- for tests i have **favored to write Feature tests** over Unit tests to ensure the flow is correct and also that gives us flexibility in changing the underlying implementation if we wanted without breaking the Order creation flow.
