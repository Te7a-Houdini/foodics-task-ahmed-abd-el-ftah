<?php

namespace App\Models;

use App\Mail\IngredientStockIsLow;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;

class Ingredient extends Model
{
    use HasFactory;

    protected  $fillable = [
        'name',
        'initial_stock',
        'current_stock',
        'low_stock_warning_sent_at',
    ];

    protected $casts = [
        'low_stock_warning_sent_at' => 'datetime',
    ];

    public function deductFromStock(int $grams): bool
    {
        return $this->update([
            'current_stock' => $this->current_stock - $grams
        ]);
    }

    public function sendWarningIfLowStock(): void
    {
        if($this->low_stock_warning_sent_at) {
            return;
        }

        if((($this->current_stock * 100) / $this->initial_stock) < 50) {
            Mail::to('merchant@gmail.com')->queue(new IngredientStockIsLow($this));

            $this->update(['low_stock_warning_sent_at' => now()]);
        }
    }
}
