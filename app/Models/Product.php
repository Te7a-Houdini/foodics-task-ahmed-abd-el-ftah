<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
    ];

    public function ingredients(): BelongsToMany
    {
        return $this->belongsToMany(Ingredient::class)->withPivot('amount');
    }

    /**
     * check if the requested quantity doesn't exceed current stock so the product can be created.
     */
    public function canFulFillQuantity(int $quantity): bool
    {
        return $this->ingredients->doesntContain(function (Ingredient $ingredient) use ($quantity) {
            return $ingredient->pivot->amount * $quantity > $ingredient->current_stock;
        });
    }
}
