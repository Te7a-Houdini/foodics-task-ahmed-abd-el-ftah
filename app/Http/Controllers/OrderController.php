<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreOrderRequest;
use App\Http\Resources\OrderResource;
use App\Models\Order;
use App\Models\Product;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    public function store(StoreOrderRequest $request)
    {
        $product = Product::find($request->input('product_id'));

        $order = null;

        DB::transaction(function () use ($request, $product, &$order) {
            $order = Order::create([
                'product_id' => $request->input('product_id'),
                'quantity' => $request->input('quantity'),
            ]);

            $product->ingredients
                ->each(fn($ingredient) => $ingredient->deductFromStock($ingredient->pivot->amount * $request->input('quantity')));
        });

        $product->ingredients->each->sendWarningIfLowStock();

        return new OrderResource($order);
    }
}
