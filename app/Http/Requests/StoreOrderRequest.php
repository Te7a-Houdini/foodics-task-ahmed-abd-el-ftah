<?php

namespace App\Http\Requests;

use App\Models\Product;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class StoreOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'product_id' => ['required', Rule::exists(Product::class, 'id')],
            'quantity' => ['required', 'numeric', 'min:1'],
        ];
    }

    protected function passedValidation()
    {
        $product = Product::find($this->input('product_id'));

        if($product->canFulfillQuantity($this->input('quantity'))) {
            return true;
        }

        throw ValidationException::withMessages([
            'product_id' => "can't fulfill order. ingredients aren't enough"
        ]);
    }
}
