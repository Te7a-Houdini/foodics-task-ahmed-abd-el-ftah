<?php

namespace Tests\Feature;

use App\Mail\IngredientStockIsLow;
use App\Models\Ingredient;
use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;

class OrderTest extends TestCase
{
    use RefreshDatabase;

    public function testItValidatesProductExistInDB()
    {
        $this->postJson(route('api.orders.store'), ['product_id' => 30])->assertJsonValidationErrorFor('product_id');
    }

    public function testQuantityIsNumericAndAboveZero()
    {
        $this->postJson(route('api.orders.store'), ['quantity' => 'invalid'])->assertJsonValidationErrorFor('quantity');
        $this->postJson(route('api.orders.store'), ['quantity' => 0])->assertJsonValidationErrorFor('quantity');
    }

    public function testProductCannotBeFulFilledIfNoIngredientsAvailable()
    {
        Mail::fake();

        $product = Product::factory()->create();
        $ingredient = Ingredient::factory()->create(['current_stock' => 200]);
        $product->ingredients()->sync([$ingredient->id => ['amount' => 200]]);

        $this->postJson(route('api.orders.store'), ['product_id' => $product->id, 'quantity' => 2])
            ->assertInvalid([
                'product_id' => "can't fulfill order. ingredients aren't enough"
            ]);

        $this->postJson(route('api.orders.store'), ['product_id' => $product->id, 'quantity' => 1])
            ->assertValid(['product_id'])
            ->assertCreated()
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'product_id',
                    'quantity',
                    'created_at'
                ],
            ]);
    }

    public function testStockIsDeductedCorrectly()
    {
        Mail::fake();

        $product = Product::factory()->create();
        $ingredient = Ingredient::factory()->create(['current_stock' => 200]);
        $product->ingredients()->sync([$ingredient->id => ['amount' => 10]]);

        $this->postJson(route('api.orders.store'), ['product_id' => $product->id, 'quantity' => 1]);
        $this->postJson(route('api.orders.store'), ['product_id' => $product->id, 'quantity' => 1]);
        $this->postJson(route('api.orders.store'), ['product_id' => $product->id, 'quantity' => 5]);

        $this->assertEquals(130 ,$ingredient->refresh()->current_stock);
    }

    public function testWarningIsSentWhenStockIsBelowFiftyPercentOneTimeOnly()
    {
        Mail::fake();

        $product = Product::factory()->create();
        $ingredient = Ingredient::factory()->create(['current_stock' => 300, 'initial_stock' => 300,]);
        $product->ingredients()->sync([$ingredient->id => ['amount' => 100]]);

        $this->postJson(route('api.orders.store'), ['product_id' => $product->id, 'quantity' => 1])->assertCreated();
        Mail::assertNothingQueued();

        $this->postJson(route('api.orders.store'), ['product_id' => $product->id, 'quantity' => 1])->assertCreated();
        Mail::assertQueued(IngredientStockIsLow::class);
        Mail::assertQueuedCount(1);

        $this->postJson(route('api.orders.store'), ['product_id' => $product->id, 'quantity' => 1])->assertCreated();
        Mail::assertQueuedCount(1);
    }
}
